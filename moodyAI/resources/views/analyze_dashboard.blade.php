@extends('app')

@section('content')

<div class="main-analayze-container">
    <div class="">
        <div class="card-body card-top">
            <p class="card-text white-text">Pritam Gaikwad <span class="red-dot"></span></p>
          </div>
          <div class="small-card-title">Suggested Activities</div>
          <div class="small-cards-holder-box">
              <div class="col-md-4 black-card small-card-1">
                  <img src="{{ asset('images/Small-card-1.jpeg') }}" alt="" srcset="" class="small-card-1-img">
                  <span class="white-text small-card-text">Explore gyms and parks around you!</span>
                  <p class="white-text learn-more-text">Learn more</p>
                  <div class="shadow-box">
                      <p class="shadow-box-text white-text"><span class="green-dot"></span>Working Out</p>
                  </div>
              </div>
              <div class="col-md-4 black-card small-card-1">
                  <img src="{{ asset('images/Small-card-2.jpeg') }}" alt="" srcset="" class="small-card-1-img">
                  <span class="white-text small-card-text">Explore classes and workshops near you!</span>
                  <p class="white-text learn-more-text">Learn more</p>
                  <div class="shadow-box">
                      <p class="shadow-box-text white-text"><span class="green-dot"></span>Dancing</p>
                  </div>
              </div>
              <div class="col-md-4 black-card small-card-1">
                  <img src="{{ asset('images/Small-card-3.jpeg') }}" alt="" srcset="" class="small-card-1-img">
                  <span class="white-text small-card-text">Read about tricks to get you to sleep in minutes!</span>
                  <p class="white-text learn-more-text">Learn more</p>
                  <div class="shadow-box">
                      <p class="shadow-box-text white-text"><span class="yellow-dot"></span>Sleeping</p>
                  </div>
              </div>

          </div>
    </div>
    <div class="right-side-long-card">
        <p class="white-text heading-right-side-card">Mood Diagnosis</p>
        <p class="white-text content-right-side-card">Name: Pritam Gaikwad</p>
        <p class="white-text content-right-side-card">Age: 21 Years</p>
        <p class="white-text content-right-side-card">Status: <span class="red-text">Angry <img src="{{ asset('images/angry-emoji.png') }}" alt="" srcset="" class="angry-emoji"> </span></p>
        <p class="white-text content-right-side-card">Time: 7:57 P.M</p>
        <p class="white-text content-right-side-card">Date: 21-11-2022</p>
        <p class="white-text content-right-side-card">Last detected: 16-11-2022, Wednesday, 8:26 P.M.</p>
        <p class="white-text content-right-side-card">Previously done activities:</p>
        <div class="col-md-4 black-card small-card-1 right-side-small-card-1">
            <img src="{{ asset('images/Small-card-1.jpeg') }}" alt="" srcset="" class="small-card-1-img">
            <p class="white-text small-card-text right-small-card-text">Result: Happy</p>
            <div class="shadow-box">
                <p class="shadow-box-text white-text"><span class="green-dot"></span>Working Out</p>
            </div>
        </div>
    </div>
</div>



@endsection

@section('page-level-scripts')
<script>
$("#home").removeClass('menu-list-item-highlighted');
$("#history").removeClass('menu-list-item-highlighted');
$("#analyze").addClass('menu-list-item-highlighted');
</script>
@endsection
