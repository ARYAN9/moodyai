@extends('app')

@section('content')
<div class="main-box">
    <div class="">
        <img src="{{ asset('images/analyze_click_btn.png') }}" alt="" srcset="" class="analyze-btn">
        {{-- <img src="{{ asset('images/analyze_arrow.png') }}" alt="" srcset="" class="analyze-arrow"> --}}
        <div class="analyze-text-btn-desc">Click here to analyze</div>
    </div>
</div>

@endsection

@section('page-level-scripts')
<script>
$("#home").removeClass('menu-list-item-highlighted');
$("#history").removeClass('menu-list-item-highlighted');
$("#analyze").addClass('menu-list-item-highlighted');
</script>
@endsection
