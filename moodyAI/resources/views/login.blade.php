<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Moody AI</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="main-container">
        <div class="right-side-img-container">
        </div>
        <div class="left-side-img-container">
        </div>

        <div class="black-box">
            <div class="main-title-login text-center">
                Moody.ai
            </div>
            <div class="text-center login-title-text">Mood based activity recommendation system</div>

            <div class="black-login-card">
                <p class="text-center">Login to Moody.ai</p>
                <p class="label-login">Email</p>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <div class="password-container">
                    <p class="label-login">Password </p>
                    <p class="forget-passwd-label">Forgot Password?</p>
                </div>
                <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="emailHelp" placeholder="Enter password">
                <div class="login-btn text-center">
                    Login
                </div>
                <div class="login-line-container">
                    <div class="col-md-5">
                        <hr class="login-line">
                    </div>
                    <div class="col-md-2">or</div>
                    <div class="col-md-5">
                        <hr class="login-line">
                    </div>
                </div>
                <div class="google-btn text-center">
                    <img src="{{ asset('images/google-icon.png') }}" alt="" srcset="" class="google-icon">
                    Sign in with Google
                </div>
                <p class="down-login-label">Don’t have an account ? <span class="create-account-label">Create one.</span></p>
            </div>
        </div>
    </div>
</body>
</html>
