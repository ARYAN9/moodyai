<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Moody AI</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="main-container">
        <div class="right-side-img-container">
        </div>
        <div class="left-side-img-container">
        </div>



            <div class="black-box black-signup-card">
                <div class="main-title-login text-center">
                    Moody.ai
                </div>
                <div class="text-center login-title-text">Mood based activity recommendation system</div>

                <div class="black-login-card">
                    <p class="text-center">Welcome! Let’s  get you started.</p>
            <p class="label-login">Email</p>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <p class="label-login">Password</p>
            <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="emailHelp" placeholder="Enter password">
            <p class="label-login">Username</p>
            <input type="text" class="form-control" id="UsernameField" aria-describedby="emailHelp" placeholder="Enter username">
            <p class="label-login">Date of birth</p>
            <input type="date" class="form-control" id="DateField" aria-describedby="emailHelp">
            <p class="label-login">Upload a picture</p>
            <input class="form-control" type="file" id="formFile">

            <div class="login-btn text-center">
                Sign up
            </div>
            <p class="down-signup-label text-center">Already have an account? <span class="create-account-label">Sign in.</span></p>
                </div>
            </div>



    </div>
</body>
</html>
