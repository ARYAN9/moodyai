<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Moody AI</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div class="mnavbar">

        <div class="col-md-12 main-nav">
            <div class="left-nav-side-container">
                <img class="robot-img" src="{{ asset('images/left-side-man.png') }}" alt="">
                <div class="navtitle col-md-2">
                    Moody.ai
                </div>
            </div>
            <ul class="menu-list">
                <li class="menu-list-item menu-list-item-highlighted" id="home">Home</li>
                <li class="menu-list-item" id="analyze">Analyze</li>
                <li class="menu-list-item" id="history">History</li>
            </ul>
            <div class="right-side-nav">
                <div class="notification">
                    <img src="{{ asset('images/notification.png') }}" alt="">
                </div>
                <div class="profile-img">
                    <img src="{{ asset('images/man_login.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>

    @yield('content')

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@yield('page-level-scripts')
</html>
